//****************************************************************************************************************
/** @file SerialCommandProcessor.cpp
 *    This file contains a Serial Command Processor class that creates a Serial Command Processor object
 *    and processes the commands coming in through the serial window. The essential methods include a 
 *    function to get the incoming string, a function to parse, analyze, and perform based on the string, 
 *    and a function to print the relevant parameters. 
 ********************************/

#include "SerialCommandProcessor.h"       // Include the Serial Command Processor header file

//-------------------------------------------------------------------------------------
/** This constructor creates an object that will take the recieved serial commands 
 *  and determine the proper processing task to run. 
 *  @param flipper1 The flipper on port 1.
 *  @param flipper2 The flipper on port 2.
 */

SerialCommandProcessor::SerialCommandProcessor(Flipper *flipper1, Flipper *flipper2) {
  this->flipper1 = flipper1;
  this->flipper2 = flipper2;
}

//-------------------------------------------------------------------------------------
/** This method sets both flippers to their default settings.  
 */
void SerialCommandProcessor::reset() {

  // Resets all parameters for both flippers to default settings
  this->flipper1->enabled = false;                      // default is flipper disabled
  this->flipper2->enabled = false;
  this->flipper1->setROM(DEFAULT_ROM);                  // default range is 0 degrees
  this->flipper2->setROM(DEFAULT_ROM);
  this->flipper1->direction = FORWARD;                  // default direction of rotation
  this->flipper2->direction = FORWARD;    
  this->flipper1->currPosition = 0;                     // position counter starts at 0
  this->flipper2->currPosition = 0;   
  this->flipper1->setSpeed(DEFAULT_FREQ);               // default rotational speed is 0 RPM
  this->flipper2->setSpeed(DEFAULT_FREQ); 
  this->flipper1->stepStyle = MICROSTEP;                // default stepping style is MICROSTEP
  this->flipper2->stepStyle = MICROSTEP; 

  // Prints out a start up prompt to initialize the flippers.
  Serial.println("\nWelcome!");
  Serial.println("Please enable each flipper using the following commands:");
  Serial.println("for port 1 flipper, type ---> 1E");
  Serial.println("for port 2 flipper, type ---> 2E");
  Serial.println("for both ports, type ---> 3E");
}

//-------------------------------------------------------------------------------------
/** This method retrieves the incoming character from the serial window and adds it to the
 *  command string variable until a newline character is received.  
 */
uint8_t SerialCommandProcessor::getIncomingChars() {

  // While a serial character is available, place the character into the inChar variable
  // for analysis. 
  while(Serial.available()) {
    char inChar = Serial.read();
    if(inChar == '\n'){
        // If the character is a newline char process and then clear the command string.
        this->processCommand(this->commandString);
        this->commandString = "";
        return 1;
    }
    else{
        // Otherwise, add it to the command string variable.
        this->commandString += inChar;
        return 0;
    }
  }
  return 0;
}

//-------------------------------------------------------------------------------------
/** This method takes in the completed command string and parses it into its individual characters.
 *  Each of these characters can then be translated into an individual action. 
 *  @param cmd The command string to be processed. 
 */
void SerialCommandProcessor::processCommand(String cmd) {  

  // Declare blank variables for a flipper object, a numerical value, and a step character.
  Flipper *f;
  int value = -1;
  char stepChar = ' ';

  // Check for reset command.
  if(cmd.charAt(0) == 'X'){
    this->reset();
    return;
  }

  // Designate the flipper to adjust based on the first char in the command string.
  else if(cmd.charAt(0) == '1') {
    f = this->flipper1;
  }
  else if (cmd.charAt(0) == '2') {
    f = this->flipper2;
  }
  else if (cmd.charAt(0) == '3') {
    // An input of '3' will control both flipper simultaneously.
    f = NULL;
  }
  else {
    // Exit processing if flipper not specified.
    return;
  }

  // If the second character is 'E', enable the appropriate flipper(s) and print a message to 
  // indicate the changes.
  if(cmd.charAt(1) == 'E'){
    if (cmd.charAt(0) == '3') {
      this->flipper1->enabled = true;
      this->flipper2->enabled = true;
      Serial.println("Both flippers are now enabled!\n");
    }
    else {
      f->enabled = true;
      Serial.println(f->name + " is now enabled!\n");
    }
  }

  // If the second character is 'D', disable the appropriate flipper(s) and print a message to 
  // indicate the changes.
  else if(cmd.charAt(1) == 'D'){
    if (cmd.charAt(0) == '3') {
      this->flipper1->enabled = false;
      this->flipper2->enabled = false;
      Serial.println("Both flippers are now disabled!\n");
    }
    else {
      f->enabled = false;
      Serial.println(f->name + " is now disabled!\n");
    }
  }

  // If the second character is 'S', it is assumed that a space char and an input character follow
  // directly after. If this is true, store the 4th character in the step char variable.
  else if(cmd.charAt(1) == 'S' && cmd.charAt(2) == ' '){
    stepChar = cmd.charAt(3);
  }

  // If the second character is 'R' or 'F', it is assumed that a space char and a numerical value
  // follow directly after. If this is true, convert the number characters into an integer value.
  else if(cmd.charAt(1) == 'R' || cmd.charAt(1) == 'F' && cmd.charAt(2) == ' '){
    value = cmd.substring(2, cmd.length()).toInt();
  }
  else { 
    Serial.println("Invalid entry. Please enter a proper command.\n"); 
  }
  
  // If the step char is not blank, that means the command was to adjust the stepping style. Based 
  // on the value of stepChar, adjust the step style to the appropriate setting for the appropriate
  // flipper and print a message to indicate the changes.
  if(stepChar != ' ') {
    if (cmd.charAt(0) == '3'){
      switch(stepChar) {
        case 'M':
          this->flipper1->setStepStyle(MICROSTEP);
          this->flipper2->setStepStyle(MICROSTEP);
          break;
        case 'I':
          this->flipper1->setStepStyle(INTERLEAVE);
          this->flipper2->setStepStyle(INTERLEAVE);
          break;
        case 'S':
          this->flipper1->setStepStyle(SINGLE);
          this->flipper2->setStepStyle(SINGLE);
          break;
        case 'D':
          this->flipper1->setStepStyle(DOUBLE);
          this->flipper2->setStepStyle(DOUBLE);
          break;
        default: 
          stepChar = ' ';
      }
      Serial.println("Step style set for both flippers.\n");
    }
    else {
      switch(stepChar) {
        case 'M':
          f->setStepStyle(MICROSTEP);
          break;
        case 'I':
          f->setStepStyle(INTERLEAVE);
          break;
        case 'S':
          f->setStepStyle(SINGLE);
          break;
        case 'D':
          f->setStepStyle(DOUBLE);
          break;
        default: 
          stepChar = ' ';
      }
      Serial.println("Step style set for" + f->name + '\n');
    }
     
  }

  // If the second char was 'R' and the value variable is >0 and <100, set the range of motion
  // to the value and print a message to indicate the changes.
  else if(cmd.charAt(1) == 'R' && value >= 0 && value <= 100){
    if (cmd.charAt(0) == '3') {
      this->flipper1->setROM(value);
      this->flipper2->setROM(value);
      Serial.println("Range set for both flippers.\n");
    }
    else {
      f->setROM(value);
      Serial.println("Range set for " + f->name + '\n');
    }
  } 

  // If the second char was 'F' and the value variable is >0 and <200, set the frequency of motion
  // to the value and print a message to indicate the changes.
  else if(cmd.charAt(1) == 'F' && value >= 0 && value <= 200){
    if (cmd.charAt(0) == '3') {
      this->flipper1->setSpeed(value);
      this->flipper2->setSpeed(value);
      Serial.println("Frequency set for both flippers.\n");
    }
    else {
      f->setSpeed(value);
      Serial.println("Frequency set for " + f->name + '\n');
    }
  } 

  // If the second char was a 'E' or 'D', do nothing and continue to the next if statement.
  else if(cmd.charAt(1) == 'E' || cmd.charAt(1) == 'D') {}
  else {
    // Otherwise, entry must have been invalid. Print message to request a proper entry.
    Serial.println("Invalid entry. Please enter a proper value.\n Range 0 --> 100. \
      \n Frequency 0 --> 200.\n"); 
  }

  // Print updated parameters for both flippers if both were changed.
  if(f == NULL){
    this->printParameters(this->flipper1);
    this->printParameters(this->flipper2);
  }
  else {
    // Print updated parameters for changed flipper.
    this->printParameters(f);
  }
}

//-------------------------------------------------------------------------------------
/** This method prints all of the flipper parameters for a given flipper. 
 *  @param my_flipper The flipper whose parameters will be printed. 
 */
void SerialCommandProcessor::printParameters(Flipper *my_flipper) {
  Serial.print("---------- Flipper Parameters: "); 
  Serial.print(my_flipper->name);
  Serial.print(" ----------\n");

  Serial.print("Enabled = ");
  Serial.println(my_flipper->enabled);
  Serial.print("Range Of Motion = ");
  Serial.println(my_flipper->rangeOfMotion); 
  Serial.print("Frequency = ");
  Serial.println(my_flipper->freq);
  Serial.print("Flipper Port = ");
  Serial.println(my_flipper->port);
  Serial.print("Step Style = ");
  String style = "";
  switch(my_flipper->stepStyle) {
    case MICROSTEP:
      style = "MICROSTEP";
      break;
    case INTERLEAVE:
      style = "INTERLEAVE";
      break;
    case SINGLE:
      style = "SINGLE";
      break;
    case DOUBLE:
      style = "DOUBLE";
      break;
    default: 
      style = "Invalid style.";
  }
  Serial.println(style + '\n');
}






