#ifndef _SERIALCOMMANDPROCESSOR_H_
#define _SERIALCOMMANDPROCESSOR_H_

#include <WString.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Arduino.h>
#include "Flipper.h"

class SerialCommandProcessor {
    public:
      SerialCommandProcessor(Flipper *flipper1, Flipper *flipper2);
      void reset();
      uint8_t getIncomingChars();
      void printParameters(Flipper *my_flipper);
    private:
      Flipper *flipper1;
      Flipper *flipper2;
      String commandString;
      void processCommand(String cmd);      
};


#endif