#ifndef _FLIPPER_H_
#define _FLIPPER_H_

#include <WString.h>
#include <Adafruit_MotorShield.h>

#define DEFAULT_ROM 0
#define DEFAULT_FREQ 0

class Flipper {
public:
  String name;
  bool enabled;
  int rangeOfMotion;
  int minPosition;
  int maxPosition;
  int freq;
  int direction;
  int currPosition;
  Adafruit_StepperMotor *motor;
  uint8_t port;
  uint8_t stepStyle;
  Flipper(Adafruit_MotorShield *AFMS, String name, uint8_t port);
  void setSpeed(int freq);
  void changeDirection();
  void step();
  void setROM(int range);
  void setStepStyle(uint8_t style);
};

#endif
