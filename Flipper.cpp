//****************************************************************************************************************
/** @file Flipper.cpp
 *    This file contains a Flipper class for the device's flippers, each powered by a Nema 11 stepper.
 *    The essential methods include a speed setting function, a direction changing function, a range
 *    setting function, a step style setting function, and a stepping function. 
***************************************************************************************************************/

#include "Flipper.h"      // Includes the Flipper header file

//----------------------------------------------------------------------------------------------------------------

/** @brief Constructor to make a flipper object
 *  \details Constructs a flipper object containing all relevant and/or adjustable parameters.
 *  @param Adafruit_MotorShield *AFMS address of the Adafruit Motor Shield in use.
 *  @param name Name given to the flipper object.
 *  @param port Motor shield port which the flipper is connected to.
 */

Flipper::Flipper(Adafruit_MotorShield *AFMS, String name, uint8_t port) {
  this->name = name;                          // name designating flipper
  this->enabled = false;                      // default is flipper disabled
  this->setROM(DEFAULT_ROM);                  // default range is 0 degrees
  this->direction = FORWARD;                  // default direction of rotation
  this->currPosition = 0;                     // position counter starts at 0
  this->motor = AFMS->getStepper(200, port);  // establish connected motor as object within flipper
  this->port = port;                          // connecting port
  this->setSpeed(DEFAULT_FREQ);               // default rotational speed is 0 RPM
  this->stepStyle = MICROSTEP;                // default stepping style is MICROSTEP
}

//-----------------------------------------------------------------------------------------------------------------
/** @brief   Method for setting the flipping speed in RPM.
 *  \details This method sets the flipper speed using the setSpeed() function already created for 
             the AFMS class within the Adafruit Stepper library.
 *  @param   freq A rotation frequency between 0 and 200 RPM.
 */

void Flipper::setSpeed(int freq) {
  this->freq = freq;                          // sets input parameter as flipper and motor frequency
  this->motor->setSpeed(freq);
}

//-----------------------------------------------------------------------------------------------------------------
/** @brief   Method for changing the direction of rotation.
 *  \details This method changes the direction of the flipper's rotation by toggling. Since the 
 *           direction is a boolean variable, it simply switches it to the opposite value. 
 */

void Flipper::changeDirection() { 
  if (this->direction == FORWARD) {
    this->direction = BACKWARD;
  } 
  else {
    this->direction = FORWARD;
  }
}

//-----------------------------------------------------------------------------------------------------------------
/** @brief   Method for actuating the flippers.
 *  \details This method actuates the flippers, provided they are enabled, by 
 *  @param   pwm_intensity A number between 0 and 1023 which gets mapped to the pwm duty cycle.
 */

void Flipper::step() {
  if (this->enabled) {
    this->currPosition += (this->direction == FORWARD) ? 1 : -1;
    this->motor->step(1, this->direction, this->stepStyle); 
    if (this->currPosition >= this->maxPosition || this->currPosition <= this->minPosition) {
      this->changeDirection();
    }
  }
}

//-----------------------------------------------------------------------------------------------------------------
/** @brief   This method sets
 *  \details Maps the duty cycle specified between 0 and 1023 to 0 to PWM TOP and puts the result in the OCnX
 *           register.
 *  @param   pwm_intensity A number between 0 and 1023 which gets mapped to the pwm duty cycle.
 */

void Flipper::setROM(int range) {
  this->rangeOfMotion = range;
  this->minPosition = -this->rangeOfMotion/(2*1.8*1);
  this->maxPosition = this->rangeOfMotion/(2*1.8*1);

}

//-----------------------------------------------------------------------------------------------------------------
/** @brief   This method sets
 *  \details Maps the duty cycle specified between 0 and 1023 to 0 to PWM TOP and puts the result in the OCnX
 *           register.
 *  @param   pwm_intensity A number between 0 and 1023 which gets mapped to the pwm duty cycle.
 */

void Flipper::setStepStyle(uint8_t style) {
  this->stepStyle = style;
}

