/* 
 *  Biomimetic Seal Flipper Test Rig Code
 *  California Polytechnic State University, San Luis Obispo
 *  Mechanical Engineering - Undergraduate Senior Project
 *  Laura Kawashiri
 *  7 June 2016
 *  
This file contins the setup() and loop() code for a program that operates the Biomimetic Seal 
Flipper Test Rig using an Arduino Uno microcontrolller and an Adafruit v2.3 Stepper Motor Shield. 
The program includes code for a Serial Command Processor and Flipper driver. The SCP takes in a 
string of commands in the serial window and parses it into individual parts to control one or both 
flippers. 
*/


#include <WString.h>                              
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include <Arduino.h>              
#include "Flipper.h"                            // Header file for flipper controls
#include "SerialCommandProcessor.h"             // Header file for serial command processing

// Connect a stepper motors with 200 steps per revolution (1.8 degree)
// to motor ports #1 (M1 and M2) and #2 (M3 and M4).

Flipper *flipper1;
Flipper *flipper2;

SerialCommandProcessor *p_SCP;

//=====================================================================================
/** The setup function sets up the components of the device with the appropriate settings.  
 *  Some test tasks are created. Then the scheduler is started up; the scheduler runs until power 
 *  is turned off or there's a reset.
 *  @return This microcontroller program doesn't return.
 */

void setup() {

  // Creates the motor shield object with the default I2C address.
  Adafruit_MotorShield *AFMS = new Adafruit_MotorShield(); 

  // Creates two new flipper objects and a new serial command processor unit for the flippers. 
  flipper1 = new Flipper(AFMS, "Flipper 1", 1);
  flipper2 = new Flipper(AFMS, "Flipper 2", 2);
  p_SCP = new SerialCommandProcessor(flipper1, flipper2);

  // Sets up Serial library at 9600 Baud.
  Serial.begin(9600);           

  // Set flippers to default settings
  p_SCP->reset();

  // Waits for a flipper to be enabled before taking any action. 
  while(p_SCP->getIncomingChars() < 1);

  // When initial command is entered, gets and processes the command.
  p_SCP->getIncomingChars();
  
  // Initializes the motor shield with the default frequency 1.6KHz.
  AFMS->begin();  
}

//=====================================================================================
/** The loop function runs the flippers and interprets serial commands when they are available. 
 *  @return This microcontroller program doesn't return.
 */

void loop() {  
  p_SCP->getIncomingChars();
  flipper1->step();
  flipper2->step();
}

